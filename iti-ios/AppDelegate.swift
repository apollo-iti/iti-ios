//
//  AppDelegate.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {    
        self.window = UIWindow(frame: UIScreen.main.bounds)

        BeagleConfig.config()
        setupNavigationBarAppearance()

        let rootViewController = LoginView().screenController()
        let navController = UINavigationController(rootViewController: rootViewController)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        return true
    }
    
    func setupNavigationBarAppearance() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 20, weight: .bold), NSAttributedStringKey.foregroundColor:UIColor.white]
    }
}
