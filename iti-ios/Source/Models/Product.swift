//
//  Product.swift
//  iti-ios
//
//  Created by Guilherme Martins on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation

struct Product: Codable {
    var id: String
    var sku: String
    var name: String
    var shortDescription: String
    var longDescription: String
    var imageUrl: String
    var price: Price
}
