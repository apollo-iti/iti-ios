//
//  CollectionDataSource.swift
//  iti-ios
//
//  Created by Guilherme Martins on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation

struct CollectionDataSource : Decodable {
    let cards: [Product]
}
