//
//  Price.swift
//  iti-ios
//
//  Created by Guilherme Martins on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation

struct Price: Codable {
    var amount: Double
    var scale: Int
    var currencyCode: String
}
