//
//  TextfieldUser.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import BeagleUI
import UIKit

struct TextfieldUser: Widget {
    
    var id: String?
    var placeholder: String
    
    var appearance: Appearance?
    var flex: Flex?
    var accessibility: Accessibility?
    
    func toView(context: BeagleContext, dependencies: RenderableDependencies) -> UIView {
        let textField = View()
        textField.borderStyle = .roundedRect
        textField.placeholder = placeholder
        textField.applyAppearance(appearance)
        textField.flex.setupFlex(flex)
        dependencies.accessibility.applyAccessibilityAttributes(accessibility, to: textField)
        return textField
    }
    
    final class View: UITextField, UITextFieldDelegate, InputValue, WidgetStateObservable {
        
        var observable = Observable<WidgetState>(value: WidgetState(value: text))
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            delegate = self
            addTarget(self, action: #selector(textChanged), for: .editingChanged)
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            delegate = self
        }
        
        func getValue() -> Any {
            return text ?? ""
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            endEditing(true)
            return true
        }
        
        @objc private func textChanged() {
            observable.value = WidgetState(value: text != "" )
//            observable.value.value = text
        }
    }
}

