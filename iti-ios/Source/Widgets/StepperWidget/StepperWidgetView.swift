//
//  StepperWidgetView.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit

class StepperWidgetView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btn_less: UIButton!
    @IBOutlet weak var btn_more: UIButton!
    @IBOutlet private weak var labelQuantity: UILabel! {
        didSet {
            self.labelQuantity.text = "\(quantity)"
        }
    }
    var quantity = 0 {
        
        didSet {
            self.labelQuantity.text = "\(quantity)"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        let name = String(describing: type(of: self))
        Bundle(for: type(of: self)).loadNibNamed(name, owner: self, options: nil)
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.frame = bounds
        btn_less.layer.cornerRadius = 6
        btn_more.layer.cornerRadius = 6
        addSubview(contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = bounds
    }
    
    @IBAction func incrementQuantity(_ sender: Any) {
        quantity += 1
        btn_less.isEnabled = true
    }
    
    @IBAction func removeQuantity(_ sender: Any) {
        
        if quantity <= 0 {
            btn_less.isEnabled = false
            return
        }
        quantity -= 1
    }

}
