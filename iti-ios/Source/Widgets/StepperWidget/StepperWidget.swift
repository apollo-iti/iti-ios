//
//  StepperWidget.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import YogaKit
import BeagleUI

struct StepperWidget: Widget {
    
    // MARK: AppearanceComponent
    var appearance: Appearance?

    // MARK: FlexComponent
    var flex: Flex?

    func toView(context: BeagleContext, dependencies: RenderableDependencies) -> UIView {
        let view = StepperWidgetView()
        view.yoga.width = YGValue(value: 100, unit: .percent)
        view.yoga.height = YGValue(value: 115, unit: .point)
        return view
    }
    
    // MARK: AccessibilityComponent
    var accessibility: Accessibility?

    // MARK: IdentifiableComponent
    var id: String?

}

struct StepperWidgetEntity: ComponentConvertibleEntity {

    // MARK: ComponentConvertibleEntity
    func mapToComponent() throws -> ServerDrivenComponent {
        return StepperWidget()
    }

}

extension StepperWidgetView: InputValue {
    func getValue() -> Any {
        return quantity
    }

}


