//
//  CollectionViewCell.swift
//  iti-ios
//
//  Created by Guilherme Martins on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    //MARK: Views
    private lazy var avatarImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "Image")
        image.layer.masksToBounds = false
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var nameLabel = buildLabel(withColor: .itiGray, ofSize: 16.0)
    private lazy var descriptionLabel = buildLabel(withColor: .itiLightGray, ofSize: 12.0)
    private lazy var priceLabel = buildLabel(withColor: .itiPink, ofSize: 14.0)
    
    //MARK: Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellLayout()
        setupCellLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Properties
    static let reuseId = String(describing: CollectionViewCell.self)
    private let spacingY: CGFloat = 5.0
    private let spacingX: CGFloat = 10.0
    
    //MARK: Public functions
    
    func setupCell(for model: Product) {
        nameLabel.text = "\(model.name)"
        descriptionLabel.text = "\(model.shortDescription)"
        
        let currency = model.price.currencyCode.formatCurrency()
        priceLabel.text = "\(currency) \(model.price.amount)"
    }
    
    //MARK: Private functions
    private func setupCellLayout() {
        addSubview(nameLabel)
        addSubview(descriptionLabel)
        addSubview(avatarImage)
        addSubview(priceLabel)
        
        [nameLabel.heightAnchor.constraint(equalToConstant: 20),
         nameLabel.widthAnchor.constraint(equalToConstant: bounds.width - spacingX*2),
         nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: spacingX),
         nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ].forEach { $0.isActive = true }
        
        [descriptionLabel.widthAnchor.constraint(equalToConstant: bounds.width - spacingX*2),
         descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: spacingY),
         descriptionLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ].forEach { $0.isActive = true }
        
        [avatarImage.heightAnchor.constraint(equalToConstant: 140),
         avatarImage.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: spacingY*2),
         avatarImage.leadingAnchor.constraint(equalTo: leadingAnchor),
         avatarImage.trailingAnchor.constraint(equalTo: trailingAnchor)
        ].forEach { $0.isActive = true }
        
        [priceLabel.widthAnchor.constraint(equalToConstant: bounds.width - spacingX*2),
         priceLabel.topAnchor.constraint(equalTo: avatarImage.bottomAnchor, constant: spacingY*2),
         priceLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        ].forEach { $0.isActive = true }
    }
    
    //MARK: Private functions
    private func setupCellLayer() {
        clipsToBounds = true
        backgroundColor = .white
        avatarImage.clipsToBounds = true
        avatarImage.contentMode = .scaleAspectFit
    }
    
    private func buildLabel(withColor color: UIColor, ofSize size:CGFloat) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: size)
        label.numberOfLines = 0
        label.textColor = color

        return label
    }
}
