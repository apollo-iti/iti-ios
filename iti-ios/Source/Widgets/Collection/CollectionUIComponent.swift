//
//  CollectionUIComponent.swift
//  iti-ios
//
//  Created by Guilherme Martins on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI

class CollectionUIComponent: UIView {
    
    //MARK: Views
    private lazy var collectionView: UICollectionView = {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: buildCollectionViewFlowLayout())
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        return collection
    }()
    
    //MARK: Properties
    private let collectionSpacing: CGFloat = 15
    private let dataSource: CollectionDataSource
    
    //MARK: Initialization
    init(dataSource: CollectionDataSource) {
        self.dataSource = dataSource
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Private functions
    ///This needs to be overrided by all custom component views
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return size
    }
    
    private func setupView() {
        registerCollectionViewCells()
        setupViewLayout()
        collectionView.reloadData()
    }
    
    private func registerCollectionViewCells() {
        self.collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: CollectionViewCell.reuseId)
    }
    
    private func setupViewLayout() {
        addSubview(collectionView)
        
        [collectionView.topAnchor.constraint(equalTo: topAnchor),
         collectionView.leadingAnchor.constraint(equalTo: leadingAnchor),
         collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
         collectionView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ].forEach { $0.isActive = true }
    }
    
    private func buildCollectionViewFlowLayout() -> UICollectionViewFlowLayout{
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.sectionInset = UIEdgeInsets(top: collectionSpacing, left: collectionSpacing, bottom: collectionSpacing, right: collectionSpacing)
        return flowLayout
    }
}

//MARK: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension CollectionUIComponent: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.reuseId, for: indexPath)
        if let collectionViewCell = cell as? CollectionViewCell {
            collectionViewCell.setupCell(for: dataSource.cards[indexPath.row])
            return collectionViewCell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let sku = dataSource.cards[indexPath.row].sku
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            if let navigatorController = topController as? UINavigationController {
                let vc = BeagleScreenViewController(viewModel: .init(screenType: .remote("/product/details/\(sku)", fallback: nil)))
                navigatorController.pushViewController(vc, animated: true)
            }
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (bounds.width / 2)-collectionSpacing*1.5, height: 249)
    }
}

