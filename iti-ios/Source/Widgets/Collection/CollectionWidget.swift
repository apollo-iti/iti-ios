//
//  CollectionWidget.swift
//  iti-ios
//
//  Created by Guilherme Martins on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI

struct CollectionWidget: Widget {
    var id: String?
    var appearance: Appearance?
    var flex: Flex?
    var accessibility: Accessibility?
    let dataSource: CollectionDataSource

    init(
        appearance: Appearance? = nil,
        flex: Flex? = nil,
        accessibility: Accessibility? = nil,
        dataSource: CollectionDataSource
    ) {
        self.appearance = appearance
        self.flex = flex
        self.accessibility = accessibility
        self.dataSource = dataSource
    }
}

struct CollectionEntity: WidgetEntity {
    var id: String?
    var flex: FlexEntity?
    var appearance: AppearanceEntity?
    var accessibility: AccessibilityEntity?
    let dataSource: CollectionDataSource
    
    func mapToComponent() throws -> ServerDrivenComponent {
        let flex = try self.flex?.mapToUIModel()
        return CollectionWidget(flex: flex, dataSource: dataSource)
    }
}

extension CollectionWidget: Renderable {
    func toView(context: BeagleContext, dependencies: RenderableDependencies) -> UIView {
        let view = CollectionUIComponent(dataSource: dataSource)
        view.flex.setupFlex(flex)
        return view
    }
}
