//
//  UIColor+Extension.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
    
    static let itiPink =  #colorLiteral(red: 0.9960784314, green: 0.3450980392, blue: 0.5254901961, alpha: 1)
    static let itiLightGray = #colorLiteral(red: 0.5647058824, green: 0.5647058824, blue: 0.5647058824, alpha: 1)
    static let itiGray = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
    
}
