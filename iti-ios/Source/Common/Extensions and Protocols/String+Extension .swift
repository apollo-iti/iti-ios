//
//  String+Extension.swift
//  iti-ios
//
//  Created by Guilherme Martins on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation

extension String {

    /**
    Get currency string code without format currency

    - returns: currency symbol string
    */
    func formatCurrency() -> String {
        let result = Locale.availableIdentifiers.map { Locale(identifier: $0) }.first { $0.currencyCode == self }
        
        return result?.currencySymbol ?? "R$"
    }
}
