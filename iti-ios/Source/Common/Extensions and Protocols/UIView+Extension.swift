//
//  UIView+Extension.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit

extension UIView {
    
    func applyiTGradient(_ colors: [UIColor]) {
        let gradient = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = [0.0, 0.5, 1.0]
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }

}
