//
//  BeagleConfig.swift
//  iti-ios
//
//  Created by Guilherme Martins on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation
import BeagleUI

class BeagleConfig {
    
    /// API version
    private static let apiVersion = "/v1"
    
    /// API Base URL
    ///
    /// - Returns: return baseURL
    private static var baseUrl: String {
        return "http://192.168.0.105:8080" + apiVersion
    }

    static func config() {
        let deepLinkHandler = DeeplinkScreenManager.shared
        deepLinkHandler["loginView"] = LoginView.self
        deepLinkHandler["detailProductView"] = DetailProductView.self
        
        let validator = ValidatorProviding()
        validator[LoginView.textValidatorName] = LoginView.textValidator
        
        let dependencies = BeagleDependencies()
        dependencies.deepLinkHandler = deepLinkHandler
        dependencies.theme = Style.theme
        dependencies.validatorProvider = validator
        dependencies.baseURL = URL(string: baseUrl)
        Beagle.dependencies = dependencies
        
        Beagle.registerCustomComponent("StepperWidget", componentType: StepperWidget.self, entityType: StepperWidgetEntity.self)
        Beagle.registerCustomComponent("collectionwidget", componentType: CollectionWidget.self, entityType: CollectionEntity.self)
    }
}
