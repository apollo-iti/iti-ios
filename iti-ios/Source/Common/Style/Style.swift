//
//  Style.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import BeagleUI
import UIKit

class Style {
    
    static var theme: AppTheme {
        
        //Todos os estilos para a aplicação
        return AppTheme(
            styles: ["form-button-orange": Style.formButtonOrange,
                     "form-button-pink": Style.formButtonPink,
                     "form-text": Style.formText,
                     "label-title": Style.labelTitle,
                     "label-Price": Style.labelPrice,
                     "label-description": Style.labelDescription,
                     "navigation-bar-gradient": navigationBarGradient]
        )
    }
    
    static func formButtonOrange() -> (UIButton?) -> Void {
        return {
            $0?.setTitleColor(.white, for: .normal)
            $0?.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .black)
            $0?.backgroundColor = $0?.isEnabled ?? false ? #colorLiteral(red: 1, green: 0.6705882353, blue: 0, alpha: 1) : #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            $0?.alpha = $0?.isHighlighted ?? false ? 0.7 : 1
        }
    }
    
    static func formButtonPink() -> (UIButton?) -> Void {
        return {
            $0?.setTitleColor(.white, for: .normal)
            $0?.backgroundColor = $0?.isEnabled ?? false ? #colorLiteral(red: 1, green: 0.4451730251, blue: 0.6062791944, alpha: 0.8470588235) : #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            $0?.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .black)
            $0?.alpha = $0?.isHighlighted ?? false ? 0.7 : 1
        }
    }
    
    static func formText() -> (UILabel?) -> Void {
        return {
            $0?.font = UIFont.systemFont(ofSize: 30, weight: .black)
            $0?.backgroundColor = .clear
        }
    }
    
    static func labelTitle() -> (UILabel?) -> Void {
        return {
            $0?.font = UIFont.systemFont(ofSize: 20, weight: .regular)
            $0?.backgroundColor = .clear
        }
    }
    
    static func labelPrice() -> (UILabel?) -> Void {
        return {
            $0?.font = UIFont.systemFont(ofSize: 20, weight: .medium)
            $0?.backgroundColor = .clear
        }
    }
    
    static func labelDescription() -> (UILabel?) -> Void {
        return {
            $0?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
            $0?.backgroundColor = .clear
        }
    }
    
    static func backgroundGradient() -> (UIView?) -> Void {
        
        return {
            $0?.backgroundColor = .clear
            $0?.applyiTGradient([
              UIColor(red: 0.92, green: 0.44, blue: 0.01, alpha: 1),
              UIColor(red: 1, green: 0.34, blue: 0.56, alpha: 1)
            ])
        }
    }
    
    static func navigationBarGradient() -> (UINavigationBar?) -> Void {
        return {
            $0?.setGradientBackground(colors: [
              UIColor(red: 0.92, green: 0.44, blue: 0.01, alpha: 1),
              UIColor(red: 1, green: 0.34, blue: 0.56, alpha: 1)
            ])
        }
    }
}
