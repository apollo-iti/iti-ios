//
//  Constants.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import Foundation

struct Constants {
    
    static let serverURL = "http://localhost:8080"
    static let login = Constants.serverURL + "/login"
    static let listProducts = Constants.serverURL + "/listProducts"
    static let detailProduct = Constants.serverURL + "/detailProduct"
    static let cartList = Constants.serverURL + "/cartList"
    
}
