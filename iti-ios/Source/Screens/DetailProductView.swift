//
//  DetailProductView.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI

struct DetailProductView: DeeplinkScreen {
    
    static var quantityValidatorName: String { return "form.quantity-not-empty" }
    static var quantityValidator: (Any) -> Bool {
        return {
            guard let quantity = $0 as? Int  else {
                return false
            }
            
            if (quantity > 0) {
                return true
            }else {
                return false
            }
        }
    }
    
    init() {}
    init(path: String, data: [String : String]?) {}
    
    func screenController() -> UIViewController {
        
        
        let imageView = NetworkImage(path: "https://abrilexame.files.wordpress.com/2019/04/m365711.jpg",
                                     contentMode: .fitCenter,
                                     id: "",flex: Flex(size: Flex.Size(height: UnitValue(value: 280, type: .real))))
        
        let labelProductName = Text("Nome do produto",
                                    style: "label-title",
                                    alignment: .left,
                                    textColor: "666666",
                                    flex: Flex(alignSelf: .flexStart, margin: Flex.EdgeValue(left: UnitValue(value: 16, type: .real))))
        
        let labelPrice = Text("R$ 10,00",
                              style: "label-price",
                              alignment: .left,
                              textColor: "FE5886",
                              flex: Flex(alignSelf: .flexEnd, margin: Flex.EdgeValue(right: UnitValue(value: 16, type: .real))))
        
        let labelDescription = Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                                    style: "label-description",
                                    alignment: .left,
                                    textColor: "919191",
                                    flex: Flex(alignSelf: .auto, margin: Flex.EdgeValue(all: UnitValue(value: 16, type: .real))))

        let stepperWidget = StepperWidget(flex: Flex(size: Flex.Size(height: UnitValue(value: 100, type: .real)),
                                                     margin: Flex.EdgeValue( all: UnitValue(value: 16, type: .real))))
        
        let formImput = FormInput(name: "stepper",
                                  validator: DetailProductView.quantityValidatorName,
                                  child: stepperWidget)
        
        let formSubmit = FormSubmit(
            child: Button(text: "Add do cart",
                          style: "form-button-pink",
                          appearance: Appearance(backgroundColor: "FE5789"), flex: Flex(size: Flex.Size(height: UnitValue(value: 66, type: .real)), margin: Flex.EdgeValue( top: UnitValue(value: 50, type: .real)))),
            enabled: false
        )
        
        let container = Container(children: [imageView,
                                             labelProductName,
                                             labelPrice,
                                             labelDescription,
                                             formImput,
                                             formSubmit],
                                  id: "",
                                  flex: Flex(grow: 1))
        
        let scrollView = ScrollView(children: [container])
        
        let form = Form(path: Constants.detailProduct,
                        method: .post,
                        child: scrollView)
        
        let screen = Screen(navigationBar: NavigationBar(title: "Product name", style: "navigation-bar-gradient", showBackButton: true, navigationBarItems: nil), content: form)
        return BeagleScreenViewController(viewModel: .init(screenType: .declarative(screen)))
    }
    
}
