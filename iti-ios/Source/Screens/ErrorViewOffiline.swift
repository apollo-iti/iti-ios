//
//  ErrorViewOffiline.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 05/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI


struct ErrorViewOffiline {
    
    var widget: Screen {
        return Screen(
            content: Container(
                children: [
                    Text("Ops! :(", alignment: .center),
                    Text(
                        "Parece que você está sem internet!\nConecte se e tente novamente",
                        alignment: .center
                    )
                    
                ],
                flex: Flex(justifyContent: .center, alignItems: .center)
            )
        )
    }
}
