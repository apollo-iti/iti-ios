//
//  LoginView.swift
//  iti-ios
//
//  Created by Marcelo I Bello on 04/03/20.
//  Copyright © 2020 Apollo. All rights reserved.
//

import UIKit
import BeagleUI

struct LoginView: DeeplinkScreen {
    
    static var textValidatorName: String { return "form.text-is-not-blank" }
    static var textValidator: (Any) -> Bool {
        return {
            let trimmed = ($0 as? String)?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? ""
            return !trimmed.isEmpty
        }
    }
    
    init() {}
    init(path: String, data: [String : String]?) {}
    
    func screenController() -> UIViewController {
        
        //Flex Textfield and Button
        let flexElements = Flex(size: Flex.Size(height: UnitValue(value: 66, type: .real)),
                                margin: Flex.EdgeValue(all: UnitValue(value: 14, type: .real)))
        
        //Image
        let image = Image(name: "icon_iti", contentMode: .fitCenter,
                          flex: Flex(size: Flex.Size(height: UnitValue(value: 89, type: .auto)),
                                     margin: Flex.EdgeValue(top: UnitValue(value: 90, type: .real))))
        
        //Label
        let label = Text("Login", style: "form-text",
                         alignment: .center,
                         textColor: "FFFFFF",
                         flex: Flex(size: Flex.Size(height: UnitValue(value: 36, type: .real)),
                                    margin: Flex.EdgeValue(all: UnitValue(value: 60, type: .real))))
        
        //Textfield
        let formImputUser = FormInput(
            name: "userName",
            required: true,
            validator: LoginView.textValidatorName,
            child: TextfieldUser(
                id: nil,
                placeholder: "Insert your user",
                appearance: nil,
                flex: flexElements,
                accessibility: nil
            )
        )
        
        //Form
        let form = Form(
            path: "/v1/login",
            method: .post,
            child: Container(
                children: [image, label, formImputUser
                    ,
                    FormSubmit(
                        child: Button(text: "Login", style: "form-button-orange", flex: flexElements),
                        enabled: false
                    )
                ],
                flex: Flex(grow: 1),
                appearance: Appearance(backgroundColor: "#FF5690")
            )
        )
        let screen = Screen(
            safeArea: SafeArea(top: false, leading: nil, bottom: false, trailing: nil),
            content: form
        )
        return BeagleScreenViewController(viewModel: .init(screenType: .declarative(screen)))
        
    }
}


