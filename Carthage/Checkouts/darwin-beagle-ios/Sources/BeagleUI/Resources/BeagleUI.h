//
//  BeagleUI.h
//  BeagleUI
//
//  Created by Daniel Tes on 10/09/19.
//  Copyright © 2019 Daniel Tes. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BeagleUI.
FOUNDATION_EXPORT double BeagleUIVersionNumber;

//! Project version string for BeagleUI.
FOUNDATION_EXPORT const unsigned char BeagleUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BeagleUI/PublicHeader.h>
